<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Cashbox;

class DefaultController extends Controller
{
    //todo check cashbox total
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('vending-static/vending-main.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }

    /**
     * @Route("/resetCashbox")
     * @return Response
     */
    public function getresetCashboxAction(){
        $conn = $this->getDoctrine()->getConnection();
        $sql = "UPDATE cashbox SET total = 11 WHERE amount = 100 LIMIT 1;
                UPDATE cashbox SET total = 24 WHERE amount = 50 LIMIT 1;
                UPDATE cashbox SET total = 0 WHERE amount = 20 LIMIT 1;
                UPDATE cashbox SET total = 99 WHERE amount = 10 LIMIT 1;
                UPDATE cashbox SET total = 200 WHERE amount = 5 LIMIT 1;
                UPDATE cashbox SET total = 11 WHERE amount = 2 LIMIT 1;
                UPDATE cashbox SET total = 23 WHERE amount = 1 LIMIT 1;";
        $query = $conn->prepare($sql);
        $query->execute();
        return new Response();
    }

    /**
     * @Route("/getCashboxSummary")
     * @return Response
     */
    public function getCashboxSummaryAction(){
        $conn = $this->getDoctrine()->getConnection();
        $sql = "SELECT * FROM cashbox";
        $query = $conn->prepare($sql);
        $query->execute();
        $row = $query->fetchAll();
        foreach($row as $result){

            $cashBox[] ='<tr>
                                <td>'.$result['id'].'</td>
                                <td>'.$result['name'].'</td>
                                <td>'.$result['amount'].'</td>
                                <td>'.$result['total'].'</td>
                              </tr>';
        }

        return $this->render('vending-static/partials/cashbox.html.twig', array( 'cashBox' => $cashBox));

    }

    public function getCashboxTotal(){
        $conn = $this->getDoctrine()->getConnection();
        $sql = "SELECT * FROM cashbox";
        $query = $conn->prepare($sql);
        $query->execute();
        $row = $query->fetchAll();

        $cashBoxTotal = 0;

        foreach($row as $result){
            $cashBoxTotal = $cashBoxTotal + ($result['total'] * $result['amount']);
        }

        return $cashBoxTotal;

    }


    public function checkChangeAvailable($amount)
    {
        if ($amount){
            $currency = $this->getDoctrine()
                ->getRepository('AppBundle:Cashbox')
                ->findOneBy(array('amount'=>$amount));
            if (false === $currency instanceof Cashbox){
                throw $this->createNotFoundException('No currency ' .$amount. ' found');
            }
        }

        if ($currency->getTotal() >= 1){
            return true;

        }
        return false;

    }

    public function decrementCurrency($amount){
        $currency = $this->getDoctrine()
            ->getRepository('AppBundle:Cashbox')
            ->findOneBy(array('amount'=>$amount));
        if (false === $currency instanceof Cashbox){
            throw $this->createNotFoundException('No currency ' .$amount. ' found');
        }

        $total = $currency->getTotal();
        $currency->setTotal($total - 1);
    }


    /**
     * @Route("/getchange/{pence}", name="getchange", defaults={"pence":null})
     */
    public function getChangeAction($pence)
    {

        $amount = (int) $pence;

        if ($this->getCashboxTotal() < $amount){
            throw $this->createNotFoundException('Insufficient change available (getcashbox total)');
        }
        $change_given = array(
            '100' => 0,
            '50' => 0,
            '20' => 0,
            '10' => 0,
            '5' => 0,
            '2' => 0,
            '1' => 0);

        $sufficient_change = true;

        while ($amount >= 1 && $sufficient_change){
            if ($amount >= 100){
                if ($this->checkChangeAvailable(100)) {
                    $amount = $amount - 100;
                    $change_given['100']++;
                    $this->decrementCurrency(100);
                    $sufficient_change = true;
                    continue;
                }
                else{
                    $sufficient_change = false;
                }
            }

            if ($amount >= 50){
                if ($this->checkChangeAvailable(50)) {
                    $amount = $amount - 50;
                    $change_given['50']++;
                    $this->decrementCurrency(50);
                    $sufficient_change = true;
                    continue;
                }
                else{
                    $sufficient_change = false;
                }
            }

            if ($amount >= 20){
                if ($this->checkChangeAvailable(20)) {
                    $amount = $amount - 20;
                    $change_given['20']++;
                    $this->decrementCurrency(20);
                    $sufficient_change = true;
                    continue;
                }
                else{
                    $sufficient_change = false;
                }
            }


            if ($amount >= 10){
                if ($this->checkChangeAvailable(10)) {
                    $amount = $amount - 10;
                    $change_given['10']++;
                    $this->decrementCurrency(10);
                    $sufficient_change = true;
                    continue;
                }
                else{
                    $sufficient_change = false;
                }
            }

            if ($amount >= 5){
                if ($this->checkChangeAvailable(5)) {
                    $amount = $amount - 5;
                    $change_given['5']++;
                    $this->decrementCurrency(5);
                    $sufficient_change = true;
                    continue;
                }
                else{
                    $sufficient_change = false;
                }
            }

            if ($amount >= 2){
                if ($this->checkChangeAvailable(2)) {
                    $amount = $amount - 2;
                    $change_given['2']++;
                    $this->decrementCurrency(2);
                    $sufficient_change = true;
                    continue;
                }
                else{
                    $sufficient_change = false;
                }
            }

            if ($amount >= 1){
                if ($this->checkChangeAvailable(1)) {
                    $amount = $amount - 1;
                    $change_given['1']++;
                    $this->decrementCurrency(1);
                    $sufficient_change = true;
                    continue;
                }
                else{
                    $sufficient_change = false;
                }
            }

        }

        if (!$sufficient_change){
            throw $this->createNotFoundException('Insufficient change available (denomination unavailable)');
        }

        else {
            $em = $this->getDoctrine()->getEntityManager();
            $em->flush();
        }

        foreach($change_given as $key => $amount){
            $currency = $this->getDoctrine()
                ->getRepository('AppBundle:Cashbox')
                ->findOneBy(array('amount'=>$key));
            if (false === $currency instanceof Cashbox){
                throw $this->createNotFoundException('No currency ' .$key. ' found');
            }

            $amount_given[] ='<tr>
                                <td>'.$currency->getName().'</td>
                                <td>'.$amount.'</td>
                              </tr>';
        }
        return $this->render('vending-static/partials/showchange.html.twig', array('change_given' => $amount_given));
    }
}
