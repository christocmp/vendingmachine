<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Cashbox;

class LoadCashbox implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $amount1 = new Cashbox();
        $amount1->setName('One pound');
        $amount1->setAmount(100);
        $amount1->setTotal(11);
        $manager->persist($amount1);

        $amount2 = new Cashbox();
        $amount2->setName('Fifty pence');
        $amount2->setAmount(50);
        $amount2->setTotal(24);
        $manager->persist($amount2);

        $amount3 = new Cashbox();
        $amount3->setName('Twenty pence');
        $amount3->setAmount(20);
        $amount3->setTotal(0);
        $manager->persist($amount3);

        $amount4 = new Cashbox();
        $amount4->setName('Ten pence');
        $amount4->setAmount(10);
        $amount4->setTotal(99);
        $manager->persist($amount4);

        $amount5 = new Cashbox();
        $amount5->setName('Five pence');
        $amount5->setAmount(5);
        $amount5->setTotal(200);
        $manager->persist($amount5);

        $amount6 = new Cashbox();
        $amount6->setName('Two pence');
        $amount6->setAmount(2);
        $amount6->setTotal(11);
        $manager->persist($amount6);

        $amount7 = new Cashbox();
        $amount7->setName('One pence');
        $amount7->setAmount(1);
        $amount7->setTotal(23);
        $manager->persist($amount7);

        $manager->flush(); //executes both queries in one go
    }

}
