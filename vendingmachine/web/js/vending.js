

$(document).on('click', '.adminButton', function(e){
    e.preventDefault();
    $('#adminWindow').modal('show');
    $.ajax({
        type: "GET",
        url: "/getCashboxSummary",
        dataType: "html",
        success: function (data) {
            if (data) {
                $('#cashboxSummary').html(data);
            }
        }
    });

});

$(document).on('click', '#getChange', function(e){
    $('#change-given').html('<p>getting change....<p>');
    e.preventDefault();
    var customAmount = $('#cash').val();

    if (customAmount > 0) {
        $.ajax({
            type: "GET",
            url: "/getchange/" + customAmount,
            dataType: "html",
            success: function (data) {
                if (data) {
                    $('#change-given').html(data);
                }
            },
            error: function () {
                $('#change-given').html('<div class="alert alert-danger" role="alert">Sorry not enough change available</div>');
            }
        });
    }

    else{
        $('#cash').effect( "shake" );
        $('#change-given').html('<div class="alert alert-danger" role="alert">Please enter your amount.</div>');

    }
});

$(document).on('click', '#resetCashBox', function(e){
    e.preventDefault();
    console.log('cashbox reset');
    $.ajax({
        type: "GET",
        url: "/resetCashbox",
        success: function(){
            $.ajax({
                type: "GET",
                url: "/getCashboxSummary",
                dataType: "html",
                success: function (data) {
                    if (data) {
                        $('#cashboxSummary').html(data);
                    }
                }
            });
        }
    });
});

