
Simple vending machine logic using Symfony 3 with bootstrap and Jquery for the frontend. 

[Live demo](http://kcom.papadopoulo.co.uk)


### Quick Setup ###

* Checkout
* Run composer install
* Apache virtualhost should point to app/web/
* Copy supplied parameters.yml containing db credentials to app/config/

### Other ###
Amazon RDS is used for the database so an internet connection will be needed.

 